all: test.o Quaternion.o
	gcc -o test test.o Quaternion.o -lm -lcmocka

test.o: test.c Quaternion.h
	gcc -c test.c

bench: bench.c Quaternion.h Quaternion.o
	gcc -o bench bench.c Quaternion.o -lm

Quaternion.o: Quaternion.c Quaternion.h
	gcc -c Quaternion.c
