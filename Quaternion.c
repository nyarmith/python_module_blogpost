#include <math.h>
#include <stdlib.h>
#include "Quaternion.h"

// put this here so we don't pollute the user's namespace
typedef struct Quaternion Quaternion;

Quaternion *make_Quaternion(void)
{
    Quaternion *ret = malloc(sizeof(Quaternion));
    init_Quaternion(ret);
    return ret;
}

void free_Quaternion(Quaternion *q)
{
    free(q);
}

void init_Quaternion(Quaternion *q)
{
    q->x = q->y = q->z = 0.0;
    q->w = 1.0;
}

Quaternion *copy_Quaternion(Quaternion *a, const Quaternion *b)
{
    *a = *b;
    return a;
}

double norm_Quaternion(const Quaternion *a)
{
    return sqrt(a->w*a->w + a->x*a->x + a->y*a->y + a->z*a->z);
}

Quaternion *negate_Quaternion(struct Quaternion *a)
{
    a->w = -a->w;
    a->x = -a->x;
    a->y = -a->y;
    a->z = -a->z;
    return a;
}

Quaternion *add_Quaternion(Quaternion *a, const Quaternion *b)
{
    a->w += b->w;
    a->x += b->x;
    a->y += b->y;
    a->z += b->z;
    return a;
}

Quaternion *sub_Quaternion(Quaternion *a, const Quaternion *b)
{
    a->w -= b->w;
    a->x -= b->x;
    a->y -= b->y;
    a->z -= b->z;
    return a;
}

Quaternion *mul_Quaternion(Quaternion *a, const Quaternion *b)
{
    static Quaternion temp;
    temp.w = a->w*b->w - a->x*b->x - a->y*b->y - a->z*b->z;
    temp.x = a->w*b->x + a->x*b->w + a->y*b->z - a->z*b->y;
    temp.y = a->w*b->y + a->y*b->w - a->x*b->z + a->z*b->x;
    temp.z = a->w*b->z + a->z*b->w + a->x*b->y - a->y*b->x;
    *a = temp;
    return a;
}

Quaternion *scale_Quaternion(Quaternion *a, double s)
{
    a->w *= s;
    a->x *= s;
    a->y *= s;
    a->z *= s;
    return a;
}

Quaternion *conj_Quaternion(Quaternion *a)
{
    a->x = -a->x;
    a->y = -a->y;
    a->z = -a->z;
    return a;
}

Quaternion *inv_Quaternion(Quaternion *a)
{
    return scale_Quaternion(conj_Quaternion(a),
        1.0/(a->w*a->w + a->x*a->x + a->y*a->y + a->z*a->z));
    // don't re-use norm here to avoid loss from squaring sqrt(n)
}

Quaternion *rot_Quaternion(Quaternion *a, const Quaternion *b)
{
    static Quaternion b_temp, bc_temp;
    b_temp = bc_temp = *b;
    *a = *mul_Quaternion(mul_Quaternion(&b_temp,a), conj_Quaternion(&bc_temp));
    return a;
    // implementation could be faster if we algebraically evaluated the general result
    // and then did only those computations and assignments necessary, factoring out
    // redundant work, e.g. the final form will not have a real component, some 
    // multiplications are repeated in different spots, etc...
    //  -- consider this an exercise to the reader :-)
}
