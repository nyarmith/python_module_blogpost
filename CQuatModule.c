#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

#include "Quaternion.h"

typedef struct {
    PyObject_HEAD;
    struct Quaternion q;
} Quaternion;

/* --- Arithmetic Operator Section  --- */
static PyObject* Quaternion_add(Quaternion *self, Quaternion *other);
static PyObject* Quaternion_subtract(Quaternion *self, Quaternion *other);
static PyObject* Quaternion_multiply(Quaternion *self, PyObject *other);
static PyObject* Quaternion_negate(Quaternion *self);

/* --- PyMethodDef Section --- */
static PyObject* Quaternion_norm(Quaternion *self);
static PyObject* Quaternion_conjugate(Quaternion *self);
static PyObject* Quaternion_invert(Quaternion *self);
static PyObject* Quaternion_rotate(Quaternion *self, PyObject *other);

/* --- Mapping Methods Section --- */
static PyObject* Quaternion_subscript(Quaternion *self, PyObject *key);
static int Quaternion_subscript_assign(Quaternion *self, PyObject *key, PyObject *v);

/* --- Type Method Section --- */
static int Quaternion_init(Quaternion *self, PyObject *args, PyObject *kwds);

static void Quaternion_dealloc(Quaternion *self){
    // since we're using python's default allocation, we must use its deallocation
    // note that this is casting self as a PyTypeObject so we can access its 
    // "free memory" method
    Py_TYPE(self)->tp_free((PyObject *)self);

    // an equivalent call for allocationg memory would be PyTYPE(self)->tp_alloc(ptr-to-PyTypeObject,0); (I think)
}

static PyObject* Quaternion_repr(Quaternion *self)
{
    static char repr[128];
    snprintf(repr,   128, "w:%f, x:%f, y:%f, z:%f", self->q.w, self->q.x, self->q.y, self->q.z);
    return PyUnicode_FromString(repr);
}

/*--- populate PyNumberMethods --- */
static PyNumberMethods Quaternion_operators = {
    .nb_add = (binaryfunc)Quaternion_add,
    .nb_subtract = (binaryfunc)Quaternion_subtract,
    .nb_multiply = (binaryfunc)Quaternion_multiply,
    .nb_negative = (unaryfunc)Quaternion_negate,
};

/* --- populate mapping methods --- */
static PyMappingMethods Quaternion_mappers = {
    .mp_subscript = (binaryfunc)Quaternion_subscript,
    .mp_ass_subscript = (objobjargproc)Quaternion_subscript_assign,
};

/*--- populate Regular Methods --- */
static PyMethodDef Quaternion_methods[] = {
    {"norm",        (PyCFunction)Quaternion_norm,         METH_NOARGS,    "Compute norm"},
    {"conjugate",   (PyCFunction)Quaternion_conjugate,    METH_NOARGS,    "Compute conjugate"},
    {"inverse",     (PyCFunction)Quaternion_invert,       METH_NOARGS,    "Compute inverse"},
    {"rotate",      (PyCFunction)Quaternion_rotate,      METH_VARARGS,   "Rotate this object by the input quaternion"},
    {NULL},
};

// here we use offsetof, as defined in stdint.h(!)
static PyMemberDef Custom_members[] = {
    {"w", T_DOUBLE, offsetof(Quaternion, q.w), 0, "real component"},
    {"x", T_DOUBLE, offsetof(Quaternion, q.x), 0, "coefficient to i"},
    {"y", T_DOUBLE, offsetof(Quaternion, q.y), 0, "coefficient to j"},
    {"z", T_DOUBLE, offsetof(Quaternion, q.z), 0, "coefficient to k"},
    {NULL}, /* array terminator */
};

static PyTypeObject QuaternionType = {
    PyVarObject_HEAD_INIT(NULL, 0)  /*object's reference count I think, mandatory boilerplate*/
    .tp_name = "CQuat.Quaternion",
    .tp_doc = PyDoc_STR("Quaternion object"),
    .tp_basicsize = sizeof(Quaternion),
    .tp_flags = Py_TPFLAGS_DEFAULT, /* bitmask of what "special fields" exist, just use default */
    .tp_itemsize = 0, /* only for variable sized objects */
    .tp_init = (initproc)Quaternion_init,
    .tp_members = Custom_members,
    .tp_methods = Quaternion_methods,
    .tp_as_number = &Quaternion_operators,
    .tp_as_mapping = &Quaternion_mappers,
    .tp_repr = (reprfunc)Quaternion_repr,

    /* create new instance using type's tp_alloc slot */
    .tp_new = PyType_GenericNew, 

     /* ".tp_alloc = PyType_GenericAlloc" is the default allocation, which uses the basicsize we
     * set earlier to create this memory and use Python's default memory allocation to 
     * allocate a new instance and initialize all its contents to NULL, we can think of this
     * field as optional*/

    /* however, the dealloc must be defined (unless our struct is empty)*/
    .tp_dealloc = (destructor)Quaternion_dealloc,

};


/* --- Python Module Definition --- */
static PyModuleDef simplequat = {
    .m_base = PyModuleDef_HEAD_INIT,
    .m_name = "simplequat",
    .m_doc = PyDoc_STR("A quaternion module with no error checking that only supports addition"),
    .m_size = -1
};

/* --- Module Initializer --- */
PyMODINIT_FUNC PyInit_CQuat(void){
    // add our definition of the type
    if (PyType_Ready(&QuaternionType) < 0) return NULL;

    // init module
    PyObject *module = PyModule_Create(&simplequat);
    if (module == NULL) return NULL;

    Py_INCREF(&QuaternionType);
    // Add the type to the module's dictionary
    if (PyModule_AddObject(module, "CQuat", (PyObject*)&QuaternionType) < 0) {
        // trigger destructors on both objects
        Py_DECREF(&QuaternionType);
        Py_DECREF(module);
        return NULL;
    }

    return module;
}

static int Quaternion_init(Quaternion *self, PyObject *args, PyObject * kwds) {
    static char *kwlist[] = {"w","x","y","z",NULL};
    // "d" means double, and "|" indicates the following arguments are optional
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|dddd", kwlist,
                                     &self->q.w,
                                     &self->q.x,
                                     &self->q.y,
                                     &self->q.z))
        return -1;

    return 0;
}

static PyObject* Quaternion_subscript(Quaternion *self, PyObject *key) {
    long index = PyLong_AsLong(key);
    switch (index)
    {
        case 0: return PyLong_FromLong(self->q.w);
        case 1: return PyLong_FromLong(self->q.x);
        case 2: return PyLong_FromLong(self->q.y);
        case 3: return PyLong_FromLong(self->q.z);
        default: return NULL;
    }
}

static int Quaternion_subscript_assign(Quaternion *self, PyObject *key, PyObject *v) {
    double value = PyFloat_AsDouble(v);
    long index = PyLong_AsLong(key);
    if (!v)
    {
        PyErr_SetString(PyExc_RuntimeError, "cannot delete items from quaternion type");
        return -1;
    }
    switch (index)
    {
        case 0: self->q.w = value;
                break;
        case 1: self->q.x = value;
                break;
        case 2: self->q.y = value;
                break;
        case 3: self->q.z = value;
                break;
        default: return -1;
    }
    return 0;
}

static PyObject* Quaternion_add(Quaternion *self, Quaternion *other) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q,&self->q);
    add_Quaternion(&ret->q, &other->q);
    return (PyObject*)ret;
}

static PyObject* Quaternion_subtract(Quaternion *self, Quaternion *other) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    return (PyObject*)ret;
}

static PyObject* Quaternion_multiply(Quaternion *self, PyObject *other) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q,&self->q);
    // are we multiplying by a numeric type? if so, scale
    if (PyLong_Check(other) || PyFloat_Check(other))
        scale_Quaternion(&ret->q, PyFloat_AsDouble(other));
    else if (PyObject_IsInstance(other, (PyObject*)&QuaternionType))
        mul_Quaternion(&ret->q, &((Quaternion*)other)->q);
    else
    {
        PyErr_SetString(PyExc_TypeError, "cannot multiply by non numeric, non quaternion type");
        return NULL;
    }

    return (PyObject*)ret;
}

static PyObject* Quaternion_negate(Quaternion *self) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q,&self->q);
    negate_Quaternion(&ret->q);
    return (PyObject*)ret;
}

static PyObject* Quaternion_norm(Quaternion *self) {
    return PyFloat_FromDouble(norm_Quaternion(&self->q));
}

static PyObject* Quaternion_conjugate(Quaternion *self) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q,&self->q);
    conj_Quaternion(&ret->q);
    return (PyObject*)ret;
}

static PyObject* Quaternion_invert(Quaternion *self) {
    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q,&self->q);
    inv_Quaternion(&ret->q);
    return (PyObject*)ret;
}

static PyObject* Quaternion_rotate(Quaternion *self, PyObject *arg) {
    PyObject *quat = NULL;
    if (!PyArg_ParseTuple(arg, "O!", &QuaternionType, &quat))
        return NULL;

    Quaternion *ret = (Quaternion*)PyObject_New(Quaternion, &QuaternionType);
    copy_Quaternion(&ret->q, &self->q);
    rot_Quaternion(&ret->q, &((Quaternion*)quat)->q);
    return (PyObject*)ret;
}
