#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "Quaternion.h"


#define RANDQUAT(q) \
    q.w = rand(); \
    q.x = rand(); \
    q.y = rand(); \
    q.z = rand();

#define RANDQARR(N,id) struct Quaternion id[N];\
    for (int i=0; i<N; ++i){ \
        RANDQUAT(id[i]); \
    }

#define BENCH_BINARY_OP(fn,N) \
    RANDQARR(N, bi##fn##a) \
    RANDQARR(N, bi##fn##b) \
    struct timespec fn##ts1, fn##ts2; \
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &fn##ts1); \
    for (int i=0; i<N; ++i) \
        fn(&bi##fn##a[i], &bi##fn##b[i]); \
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &fn##ts2); \
    double time##fn = (fn##ts2.tv_sec - fn##ts1.tv_sec) * 1e3; \
    time##fn += (fn##ts2.tv_nsec - fn##ts1.tv_nsec) / 1e6; \
    printf("average for %s : %fms\n", #fn, time##fn / N);

#define BENCH_UNARY_OP(fn,N) \
    RANDQARR(N, un##fn) \
    struct timespec fn##ts1, fn##ts2; \
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &fn##ts1); \
    for (int i=0; i<N; ++i) \
        fn(&un##fn[i]); \
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &fn##ts2); \
    double time##fn = (fn##ts2.tv_sec - fn##ts1.tv_sec) * 1e3; \
    time##fn += (fn##ts2.tv_nsec - fn##ts1.tv_nsec) / 1e6; \
    printf("average for %s : %fms\n", #fn, time##fn / N);

#define SAMPLE_SIZE 10000

int main(void){
    srand(1);

    BENCH_UNARY_OP(init_Quaternion, SAMPLE_SIZE);
    BENCH_UNARY_OP(norm_Quaternion, SAMPLE_SIZE);
    BENCH_UNARY_OP(negate_Quaternion, SAMPLE_SIZE);
    BENCH_UNARY_OP(inv_Quaternion, SAMPLE_SIZE);
    BENCH_UNARY_OP(conj_Quaternion, SAMPLE_SIZE);

    BENCH_BINARY_OP(copy_Quaternion, SAMPLE_SIZE);
    BENCH_BINARY_OP(add_Quaternion, SAMPLE_SIZE);
    BENCH_BINARY_OP(sub_Quaternion, SAMPLE_SIZE);
    BENCH_BINARY_OP(mul_Quaternion, SAMPLE_SIZE);
    BENCH_BINARY_OP(rot_Quaternion, SAMPLE_SIZE);
}
