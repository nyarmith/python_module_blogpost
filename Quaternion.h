struct Quaternion{
    double w; // real component
    double x; // i coefficient
    double y; // j coefficient
    double z; // k coefficient
};

/* create an uninitialized quaternion */
struct Quaternion* make_Quaternion(void);

/* free a quaternion created with make_Quaternion */
void free_Quaternion(struct Quaternion *q);

/* initialize quaternion to 1+0i+0j+0k, aka 1 */
void init_Quaternion(struct Quaternion *q);

/* copy b to a, returns pointer to a*/
struct Quaternion *copy_Quaternion(struct Quaternion *a, const struct Quaternion *b);

/* get norm of quaternion a*/
double norm_Quaternion(const struct Quaternion *a);

/* make a = -a, returns pointer to a */
struct Quaternion *negate_Quaternion(struct Quaternion *a);

/* add b to a, returns pointer to a*/
struct Quaternion *add_Quaternion(struct Quaternion *a, const struct Quaternion *b);

/* subtract b from a, returns pointer to a*/
struct Quaternion *sub_Quaternion(struct Quaternion *a, const struct Quaternion *b);

/* multiply a by b, storing the result in a, returns pointer to a */
struct Quaternion *mul_Quaternion(struct Quaternion *a, const struct Quaternion *b);

/* scale a by s, returns pointer to a*/
struct Quaternion *scale_Quaternion(struct Quaternion *a, double s);

/* make a its conjugate, returns pointer to a*/
struct Quaternion *conj_Quaternion(struct Quaternion *a);

/* invert  a, returns pointer to a*/
struct Quaternion *inv_Quaternion(struct Quaternion *a);

/* rotate quaternion a by b, returns pointer to a */
struct Quaternion *rot_Quaternion(struct Quaternion *a, const struct Quaternion *b);
