#include <float.h>

// headers required by cmocka
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>

#include <cmocka.h>

#include "Quaternion.h"

static void test_creation(void **state)
{
    struct Quaternion *q = NULL;
    q = make_Quaternion();
    assert_false(q == NULL);
    q->w = 0;
    q->z = -1;
    init_Quaternion(q);
    assert_true(q->w == 1.0);
    assert_true(q->z == 0.0);

    struct Quaternion q2;
    copy_Quaternion(&q2, q);
    assert_memory_equal(&q2, q, sizeof(q2));

    free_Quaternion(q);
}

// add, subtract, negate, norm, scale
static void test_easy_operations(void **state)
{
    struct Quaternion q1 = {0.1, 0.2, 0.3, 0.4};
    struct Quaternion q2;
    negate_Quaternion(copy_Quaternion(&q2,&q1));
    assert_float_equal(q1.w, -q2.w, FLT_EPSILON);
    assert_float_equal(q1.x, -q2.x, FLT_EPSILON);
    assert_float_equal(q1.y, -q2.y, FLT_EPSILON);
    assert_float_equal(q1.z, -q2.z, FLT_EPSILON);

    sub_Quaternion(&q2, &q1);
    negate_Quaternion(&q2);
    assert_float_equal(q1.w*2.0f, q2.w, FLT_EPSILON);
    assert_float_equal(q1.x*2.0f, q2.x, FLT_EPSILON);
    assert_float_equal(q1.y*2.0f, q2.y, FLT_EPSILON);
    assert_float_equal(q1.z*2.0f, q2.z, FLT_EPSILON);

    add_Quaternion(&q2, scale_Quaternion(&q1, -2.0));
    assert_float_equal(q2.w, 0.0f, FLT_EPSILON);
    assert_float_equal(q2.x, 0.0f, FLT_EPSILON);
    assert_float_equal(q2.y, 0.0f, FLT_EPSILON);
    assert_float_equal(q2.z, 0.0f, FLT_EPSILON);


    struct Quaternion q3 = {.w = 2.0, .x=2.0, .y=2.0, .z=2.0};
    assert_float_equal(norm_Quaternion(&q3), 4.0f, FLT_EPSILON);

    scale_Quaternion(&q3, 2.0);
    assert_float_equal(norm_Quaternion(&q3), 8.0f, FLT_EPSILON);
}

// conjugate, inverse, multiplication, rotation
static void test_hard_operations(void **state)
{
    struct Quaternion q1 = {0.1, 0.2, 0.3, 0.4}, q2, q3, q4;
    q4=q3=q2=q1;

    double N = norm_Quaternion(&q1);

    // check q*q'
    conj_Quaternion(&q3);
    mul_Quaternion(&q2, &q3);
    assert_float_equal(q2.w, N*N, FLT_EPSILON);
    assert_float_equal(q2.x, 0, FLT_EPSILON);
    assert_float_equal(q2.y, 0, FLT_EPSILON);
    assert_float_equal(q2.z, 0, FLT_EPSILON);

    // check inverse calculation
    scale_Quaternion(&q3, 1.0/(N*N));
    inv_Quaternion(&q4);
    assert_float_equal(q3.w, q4.w, FLT_EPSILON);
    assert_float_equal(q3.x, q4.x, FLT_EPSILON);
    assert_float_equal(q3.y, q4.y, FLT_EPSILON);
    assert_float_equal(q3.z, q4.z, FLT_EPSILON);

    // check inverse multiplication gives us identity
    mul_Quaternion(&q1, &q4);
    assert_float_equal(q1.w, 1.0f, FLT_EPSILON);
    assert_float_equal(q1.x, 0.0f, FLT_EPSILON);
    assert_float_equal(q1.y, 0.0f, FLT_EPSILON);
    assert_float_equal(q1.z, 0.0f, FLT_EPSILON);
    
    // check rotation with identity
    rot_Quaternion(&q1, &q2);
    assert_float_equal(q1.w, q2.w*q2.w, FLT_EPSILON);
    assert_float_equal(q1.x, q2.x, FLT_EPSILON);
    assert_float_equal(q1.y, q2.y, FLT_EPSILON);
    assert_float_equal(q1.z, q2.z, FLT_EPSILON);

}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_creation),
        cmocka_unit_test(test_easy_operations),
        cmocka_unit_test(test_hard_operations),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
