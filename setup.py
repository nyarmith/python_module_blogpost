from distutils.core import setup, Extension

quat_module = Extension('CQuat',
        sources=['CQuatModule.c','Quaternion.c'])

setup(name="CQuat",
    version="1.0",
    description = 'A Python Quaternion Extension',
      ext_modules=[quat_module])
