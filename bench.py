from CQuat import CQuat
from random import random
import time


def rand_CQuat():
    q = CQuat()
    q.w = random()*20000
    q.x = random()*20000
    q.y = random()*20000
    q.z = random()*20000
    return q

def time_binary_op(func, N):
    ops = [(rand_CQuat(),rand_CQuat()) for i in range(0,N)]

    t1 = time.time()
    for i in range(0,N):
        func(ops[i][0], ops[i][1])

    t2 = time.time()
    print(f"%s avg time %s" % (func.__name__, (t2 - t1)*(1e9/N)))

def time_unary_op(func, N):
    ops = [rand_CQuat() for i in range(0,N)]

    t1 = time.time()
    for i in range(0,N):
        func(ops[i])

    t2 = time.time()
    print(f"%s avg time %s" % (func.__name__, (t2 - t1)*(1e9/N)))

if __name__ == '__main__':
    # generate input quaternions
    N = 10000
    time_unary_op(CQuat.norm,N)
    time_unary_op(CQuat.__neg__,N)
    time_unary_op(CQuat.inverse,N)
    time_unary_op(CQuat.conjugate,N)


    time_binary_op(CQuat.__add__,N)
    time_binary_op(CQuat.__sub__,N)
    time_binary_op(CQuat.__mul__,N)
    time_binary_op(CQuat.rotate,N)
